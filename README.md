# 3ctiming

Instalar Docker: https://download.docker.com/win/edge/Docker%20for%20Windows%20Installer.exe

#Começando
**_Subir a aplicação:_** docker-compose up -d --build

**_Construir a aplicação:_** docker-compose exec php-fpm composer install

**_Gerar tabelas banco de dados:_** docker-compose exec php-fpm php bin/console doc:mig:mig -n

**_Gerar uma versão das alterações do banco de dados:_** docker-compose exec php-fpm php bin/console doc:mig:diff. Após gerar a nova versão, gerar tabelas novamente. 

**_Ex: após todo pull, gerar o mig_**

**_Após alterar ou criar uma entidade: gerar o diff e logo após executar o mig_**

**_Documentação do Migrations:_** http://symfony.com/doc/master/bundles/DoctrineMigrationsBundle/index.html

**_Criar usuário super admin:_** docker-compose exec php-fpm php bin/console fos:user:create --super-admin

**_Rota para admin:_**  /admin

**_Restaurar banco: docker-compose exec -T mysql sh -c 'exec mysql -uroot -pmysql 3ctimming' < ~/Downloads/hg3ct625_new3ctimming\ \(2\).sql 