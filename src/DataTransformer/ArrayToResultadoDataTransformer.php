<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 11/11/2018
 * Time: 10:46
 */

namespace App\DataTransformer;


use App\Entity\Resultado;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ArrayToResultadoDataTransformer implements DataTransformerInterface
{

    const NUMERO = "Nr.";
    const NOME = "Nome";
    const FAIXA = "Faixa";
    const TEMPO_LIQUIDO = "T.Líquido";
    const TEMPO_BRUTO = "T.Bruto";
    const EQUIPE = "Equipe";
    const CIDADE = "Cidade";
    const POSICAO_GERAL = "POS_GERAL";
    const POSICAO_FAIXA = "POS_FAIXA";
    const PREMIACAO_GERAL = "PREM_GERAL";
    const PREMIACAO_FAIXA = "PREM_FAIXA";
    const APELIDO = "Apelido";
    const SEXO = "Sexo";
    const DISTANCIA = "DISTANCIA";

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $arrayResultado
     * @return Resultado
     */
    public function transform($arrayResultado) {
        $resultado = new Resultado();
        foreach ($arrayResultado as $key => $valor) {
            switch (mb_strtolower($key)) {
                case mb_strtolower(self::NUMERO):
                    $resultado->setNumero($valor);
                    break;
                case mb_strtolower(self::NOME):
                    $resultado->setNome($valor);
                    break;
                case mb_strtolower(self::FAIXA):
                    $resultado->setFaixa($valor);
                    break;
                case mb_strtolower(self::TEMPO_LIQUIDO):
                    $resultado->setTempoLiquido($this->stringToDateInterval($valor));
                    break;
                case mb_strtolower(self::TEMPO_BRUTO):
                    $resultado->setTempoBruto($this->stringToDateInterval($valor));
                    break;
                case mb_strtolower(self::EQUIPE):
                    $resultado->setEquipe($valor);
                    break;
                case mb_strtolower(self::CIDADE):
                    $resultado->setCidade($valor);
                    break;
                case mb_strtolower(self::PREMIACAO_GERAL):
                    $resultado->setPremiacaoGeral($valor);
                    break;
                case mb_strtolower(self::PREMIACAO_FAIXA):
                    $resultado->setPremiacaoFaixa($valor);
                    break;
                case mb_strtolower(self::POSICAO_GERAL):
                    $resultado->setPosicaoGeral($valor);
                    break;
                case mb_strtolower(self::POSICAO_FAIXA):
                    $resultado->setPosicaoFaixa($valor);
                    break;
                case mb_strtolower(self::APELIDO):
                    $resultado->setApelido($valor);
                    break;
                case mb_strtolower(self::SEXO):
                    $resultado->setSexo($valor);
                    break;
                case mb_strtolower(self::DISTANCIA):
                    $resultado->setDistancia($valor);
                    break;
            }
        }
        return $resultado;
    }

    /**
     * @param Resultado $resultado
     * @return array
     */
    public function reverseTransform($resultado) {

        $arrayResultado = [];
        $arrayResultado[self::NUMERO] = $resultado->getNumero();
        $arrayResultado[self::NOME] = $resultado->getNome();
        $arrayResultado[self::APELIDO] = $resultado->getApelido();
        $arrayResultado[self::SEXO] = $resultado->getSexo();
        $arrayResultado[self::FAIXA] = $resultado->getFaixa();
        $arrayResultado[self::POSICAO_GERAL] = $resultado->getPosicaoGeral();
        $arrayResultado[self::POSICAO_FAIXA] = $resultado->getPosicaoFaixa();
        $arrayResultado[self::TEMPO_BRUTO] = $resultado->getTempoBruto() ? $resultado->getTempoBruto()->format('%H:%I:%S') : null;
        $arrayResultado[self::TEMPO_LIQUIDO] = $resultado->getTempoLiquido() ? $resultado->getTempoLiquido()->format('%H:%I:%S') : null;
        $arrayResultado[self::EQUIPE] = $resultado->getEquipe();
        $arrayResultado[self::CIDADE] = $resultado->getCidade();
        $arrayResultado[self::PREMIACAO_GERAL] = $resultado->getPremiacaoGeral();
        $arrayResultado[self::PREMIACAO_FAIXA] = $resultado->getPremiacaoFaixa();

        return $arrayResultado;
    }

    private function stringToDateInterval(string $tempo): \DateInterval {
        if (strlen($tempo) == 5) {
            $partes = explode(':', $tempo);
            return new \DateInterval('P0Y0DT' . $partes[0] . 'M' . $partes[1] . 'S');
        } else {
            $partes = explode(':', $tempo);
            return new \DateInterval('P0Y0DT' . $partes[0] . 'H' . $partes[1] . 'M' . $partes[2] . 'S');
        }
    }

}