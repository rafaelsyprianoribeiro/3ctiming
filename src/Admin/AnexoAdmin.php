<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 27/08/2018
 * Time: 19:58
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin as Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AnexoAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', FileType::class, [
                'required' => false
            ])
            ->add('filename', null, [
                'required' => false,
                'disabled' => true,
            ])
            ->add('categoria', null, [
                'required' => true
            ])
        ;
    }

    public function prePersist($anexo)
    {
        $this->manageFileUpload($anexo);
    }

    public function preUpdate($anexo)
    {
        $this->manageFileUpload($anexo);
    }

    private function manageFileUpload($anexo)
    {
        if ($anexo->getFile()) {
            $anexo->refreshUpdated();
        }
    }
}