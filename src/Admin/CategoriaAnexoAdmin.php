<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 16/08/2018
 * Time: 07:36
 */

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin as Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class CategoriaAnexoAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection): void
    {
//        $collection->remove('show');
//        $collection->remove('delete');
//        $collection->remove('export');
//        $collection->remove('create');
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
            ->add('nome');
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('nome');
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper

            ->add('nome')
            ->add('_ações', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                ]
            ]);
    }

}