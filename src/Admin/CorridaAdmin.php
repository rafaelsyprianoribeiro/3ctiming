<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 16/08/2018
 * Time: 07:36
 */

namespace App\Admin;

use App\Entity\Anexo;
use App\Entity\Corrida;
use Sonata\AdminBundle\Admin\AbstractAdmin as Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\Form\Type\DatePickerType;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use App\Entity\Image;

class CorridaAdmin extends Admin
{

    protected $datagridValues = [
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'data',
    ];

    protected $baseRoutePattern = 'corrida';

    /**
     * @param ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
            ->add('nome')
            ->add('data')
            ->add('cidade')
            ->add('linkRegulamento')
            ->add('linkOrganizador');
    }

    /**
     * @param FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        /** @var Image $image */
        $image = $this->getSubject()->getImagem();

        /** @var Image $image */
        $imageCertificado = $this->getSubject()->getImagemCertificado();


        // use $fileFieldOptionsImage so we can add other options to the field
        $fileFieldOptionsImage = [
            'required' => false
        ];
        if ($image && ($webPath = $image->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/' . $webPath;
            // add a 'help' option containing the preview's img tag
            $fileFieldOptionsImage = [
                'help' => '<img width="180" src="' . $fullPath . '" class="admin-preview" />',
                'required' => false
            ];
        }

        $fileFieldOptionsImageCertificado = [
            'required' => false
        ];
        if ($imageCertificado && ($webPath = $imageCertificado->getWebPath())) {
            // get the container so the full path to the image can be set
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath() . '/' . $webPath;
            // add a 'help' option containing the preview's img tag
            $fileFieldOptionsImageCertificado = [
                'help' => '<img width="180" src="' . $fullPath . '" class="admin-preview" />',
                'required' => false
            ];
        }

        /** @var Anexo[] $anexos */
        $anexos = $this->getSubject()->getAnexos();

        $formMapper
            ->add('nome')
            ->add('data', DatePickerType::class, [
                'format' => \IntlDateFormatter::SHORT,
                'dp_side_by_side' => true,
                'dp_use_current' => false,
                'dp_collapse' => false,
                'dp_calendar_weeks' => false,
                'dp_view_mode' => 'days',
                'dp_min_view_mode' => 'days',
                'dp_show_today' => true,
            ])
            ->add('cidade')
            ->add('linkRegulamento', UrlType::class, [
                'required' => false
            ])
            ->add('linkOrganizador', UrlType::class, [
                'required' => false
            ])
            ->add('imagem', AdminType::class, $fileFieldOptionsImage)
            ->add('imagemCertificado', AdminType::class, $fileFieldOptionsImageCertificado)
            ->add('anexos', CollectionType::class, [
                'required' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
            ])
            ->add('tituloAnexos', null, [
                'label' => 'Título do link do anexo na home'
            ])
            ->add('mostrarAnexosHome',null, [
                'label' => 'Mostrar anexos na home'
            ])
        ;
    }

    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('nome')
            ->add('data', null, [
                'format' => 'd/m/Y'
            ])
            ->add('cidade')
            ->add('_ações', 'actions', [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                    'action_name' => array('template' => 'Admin/Corrida/list__action_gerenciar.html.twig'),
                ]
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
            ->add('nome')
            ->add('data')
            ->add('cidade');
    }

    public function prePersist($corrida) {
        $this->manageEmbeddedImageAdmins($corrida);
        $this->manageEmbeddedAnexoAdmins($corrida);
    }

    public function preUpdate($corrida) {
        $this->manageEmbeddedImageAdmins($corrida);
        $this->manageEmbeddedAnexoAdmins($corrida);
    }

    public function validate(ErrorElement $errorElement, $object) {
        if (is_null($object->getImagem()) && !in_array($object->getImagem()->getContentType(), ['image/pjpeg', 'image/jpeg', 'image/png', 'image/x-png'])) {
            $errorElement
                ->with('imagem')
                ->addViolation('Tipo de Imagem inválido')
                ->end();
        };
        if (is_null($object->getImagemCertificado()) && !in_array($object->getImagemCertificado()->getContentType(), ['image/pjpeg', 'image/jpeg', 'image/png', 'image/x-png'])) {
            $errorElement
                ->with('imagemCertificado')
                ->addViolation('Tipo de Imagem inválido')
                ->end();
        };

    }

    private function manageEmbeddedImageAdmins($corrida) {
        // Cycle through each field
        foreach ($this->getFormFieldDescriptions() as $fieldName => $fieldDescription) {
            // detect embedded Admins that manage Images
            if ($fieldDescription->getType() === AdminType::class &&
                ($associationMapping = $fieldDescription->getAssociationMapping()) &&
                $associationMapping['targetEntity'] === 'App\Entity\Image'
            ) {
                $getter = 'get' . $fieldName;
                $setter = 'set' . $fieldName;

                /** @var Image $image */
                $image = $corrida->$getter();
                if ($image) {
                    if ($image->getFile() && $image->getFile()->getClientOriginalName()) {
                        // update the Image to trigger file management
                        $image->refreshUpdated();
                    } elseif (!$image->getFile() && !$image->getFilename()) {
                        // prevent Sf/Sonata trying to create and persist an empty Image
                        $corrida->$setter(null);
                    }
                }
            }
        }
    }

    /**
     * @param $corrida Corrida
     */
    private function manageEmbeddedAnexoAdmins($corrida) {
        foreach ($corrida->getAnexos() as $anexo) {
            if ($anexo->getFile() && $anexo->getFile()->getClientOriginalName()) {
                // update the Image to trigger file management
                $anexo->refreshUpdated();
            }
        }
    }

}