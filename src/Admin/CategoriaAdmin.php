<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 16/08/2018
 * Time: 07:36
 */

namespace App\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin as Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class CategoriaAdmin extends Admin
{
    public $corridaId;

    private $em;

    public function __construct(string $code, string $class, string $baseControllerName, EntityManagerInterface $em) {
        $this->em = $em;
        parent::__construct($code, $class, $baseControllerName);
    }

    /**
     * @param \Sonata\AdminBundle\Show\ShowMapper $showMapper
     *
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper) {
        $showMapper
            ->add('corrida')
            ->add('nome');
    }

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $formMapper
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('corrida')
            ->add('nome');
    }

   protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->add('list', 'list/{id}');
    }



    /**
     * @param \Sonata\AdminBundle\Datagrid\ListMapper $listMapper
     *
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('Nome')
            ->add('corrida')
            ->add('resultados')
            ->add('_ações', 'actions', [
                'actions' => [
                    'delete' => [],
                ]
            ]);
        $corridaId = $this->request->attributes->get('id');
        $this->corrida = $this->em->getRepository('App:Corrida')
            ->find($corridaId);
    }


    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->eq($query->getRootAliases()[0] . '.corrida', ':id')
        );
        $query->setParameter('id', $this->request->attributes->get('id'));
        return $query;
    }

}