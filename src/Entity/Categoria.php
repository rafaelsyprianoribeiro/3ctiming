<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Corrida", inversedBy="categorias")
     */
    private $corrida;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nome;

    /**
     * @ORM\Column(type="float", options={ "default" = 0 })
     */
    private $distancia;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Resultado", mappedBy="categoria", cascade={"all"})
     */
    private $resultados;

    public function __construct() {
        $this->resultados = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getCorrida(): ?Corrida {
        return $this->corrida;
    }

    public function setCorrida(?Corrida $corrida): self {
        $this->corrida = $corrida;

        return $this;
    }

    public function getNome(): ?string {
        return $this->nome;
    }

    public function setNome(string $nome): self {
        $this->nome = $nome;

        return $this;
    }

    public function getDistancia(): ?float {
        return $this->distancia;
    }

    public function setDistancia(float $distancia): self {
        $this->distancia = $distancia;

        return $this;
    }

    /**
     * @return Collection|Resultado[]
     */
    public function getResultados(): Collection {
        return $this->resultados;
    }

    public function addResultado(Resultado $resultado): self {
        if (!$this->resultados->contains($resultado)) {
            $this->resultados[] = $resultado;
            $resultado->setCategoria($this);
        }

        return $this;
    }

    public function removeResultado(Resultado $resultado): self {
        if ($this->resultados->contains($resultado)) {
            $this->resultados->removeElement($resultado);
            // set the owning side to null (unless already changed)
            if ($resultado->getCategoria() === $this) {
                $resultado->setCategoria(null);
            }
        }

        return $this;
    }

    public function __toString(): ?string {
        return $this->nome ?? "";
    }
}
