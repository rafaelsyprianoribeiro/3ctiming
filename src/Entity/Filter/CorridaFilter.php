<?php
/**
 * Created by PhpStorm.
 * User: robson.faveri
 * Date: 25/10/2017
 * Time: 11:37
 */

namespace App\Entity\Filter;



class CorridaFilter
{

    private $nome;

    private $data;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     * @return CorridaFilter
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return CorridaFilter
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }





}