<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 27/08/2018
 * Time: 19:57
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{

    const SERVER_PATH_TO_IMAGE_FOLDER = 'images/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Image
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        $info = new \SplFileInfo($this->getFile()->getClientOriginalName());
        $newFileName = uniqid('imagem_corrida_') . '.' . $info->getExtension();
        $this->getFile()->move(self::SERVER_PATH_TO_IMAGE_FOLDER, $newFileName);
        $this->filename = $newFileName;
        $this->setFile(null);
    }

    public function getContentType()
    {
        return $this->getFile()->getMimeType();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire
     */
    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Image
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    public function getWebPath()
    {
        return null === $this->filename
            ? null
            : self::SERVER_PATH_TO_IMAGE_FOLDER . $this->filename;
    }

    public function getFilename()
    {
        return $this->filename;
    }
}