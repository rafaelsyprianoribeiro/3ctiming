<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 27/08/2018
 * Time: 19:57
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnexoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Anexo
{

    const SERVER_PATH_TO_ANEXO_FOLDER = 'anexos/';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Corrida", inversedBy="anexos")
     */
    private $corrida;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoriaAnexo")
     */
    private $categoria;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Anexo
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        $info = new \SplFileInfo($this->getFile()->getClientOriginalName());
        $newFileName = $info->getFilename();
        $this->getFile()->move(self::SERVER_PATH_TO_ANEXO_FOLDER, $newFileName);
        $this->filename = $newFileName;
        $this->setFile(null);
    }

    public function getContentType()
    {
        return $this->getFile()->getMimeType();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
        $this->refreshUpdated();
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeFile() {
        if (file_exists(self::SERVER_PATH_TO_ANEXO_FOLDER . $this->getFilename()))
            unlink(self::SERVER_PATH_TO_ANEXO_FOLDER . $this->getFilename());
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire
     */
    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Anexo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    public function getWebPath()
    {
        return null === $this->filename
            ? null
            : self::SERVER_PATH_TO_ANEXO_FOLDER . $this->filename;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return mixed
     */
    public function getCorrida() {
        return $this->corrida;
    }

    /**
     * @param mixed $corrida
     * @return Anexo
     */
    public function setCorrida($corrida) {
        $this->corrida = $corrida;
        return $this;
    }


    public function __toString() {
        return $this->getFilename();
    }

    public function getCategoria(): ?CategoriaAnexo
    {
        return $this->categoria;
    }

    public function setCategoria(?CategoriaAnexo $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }
}