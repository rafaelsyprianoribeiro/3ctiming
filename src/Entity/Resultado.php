<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultadoRepository")
 */
class Resultado
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="resultados")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apelido;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sexo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $faixa;

    /**
     * @ORM\Column(type="integer")
     */
    private $posicaoGeral;

    /**
     * @ORM\Column(type="integer")
     */
    private $posicaoFaixa;

    /**
     * @ORM\Column(type="dateinterval", nullable=true)
     */
    private $tempoBruto;

    /**
     * @ORM\Column(type="dateinterval", nullable=true)
     */
    private $tempoLiquido;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $equipe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @ORM\Column(type="boolean")
     */
    private $premiacaoGeral;

    /**
     * @ORM\Column(type="boolean")
     */
    private $premiacaoFaixa;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="float", options={ "default" = 0 }, nullable=true)
     */
    private $distancia;

    public function getId(): ?int {
        return $this->id;
    }

    public function getCategoria(): ?Categoria {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self {
        $this->categoria = $categoria;

        return $this;
    }

    public function getApelido(): ?string {
        return $this->apelido;
    }

    public function setApelido(?string $apelido): self {
        $this->apelido = $apelido;

        return $this;
    }

    public function getSexo(): ?string {
        return $this->sexo;
    }

    public function setSexo(?string $sexo): self {
        $this->sexo = $sexo;

        return $this;
    }

    public function getFaixa(): ?string {
        return $this->faixa;
    }

    public function setFaixa(string $faixa): self {
        $this->faixa = $faixa;

        return $this;
    }

    public function getPosicaoGeral(): ?int {
        return $this->posicaoGeral;
    }

    public function setPosicaoGeral(int $posicaoGeral): self {
        $this->posicaoGeral = $posicaoGeral;

        return $this;
    }

    public function getPosicaoFaixa(): ?int {
        return $this->posicaoFaixa;
    }

    public function setPosicaoFaixa(int $posicaoFaixa): self {
        $this->posicaoFaixa = $posicaoFaixa;

        return $this;
    }

    public function getTempoBruto(): ?\DateInterval {
        return $this->tempoBruto;
    }

    public function setTempoBruto(?\DateInterval $tempoBruto): self {
        $this->tempoBruto = $tempoBruto;

        return $this;
    }

    public function getTempoLiquido(): ?\DateInterval {
        return $this->tempoLiquido;
    }

    public function setTempoLiquido(?\DateInterval $tempoLiquido): self {
        $this->tempoLiquido = $tempoLiquido;

        return $this;
    }

    public function getEquipe(): ?string {
        return $this->equipe;
    }

    public function setEquipe(?string $equipe): self {
        $this->equipe = $equipe;

        return $this;
    }

    public function getCidade(): ?string {
        return $this->cidade;
    }

    public function setCidade(?string $cidade): self {
        $this->cidade = $cidade;

        return $this;
    }

    public function getPremiacaoGeral(): ?bool {
        return $this->premiacaoGeral;
    }

    public function setPremiacaoGeral(bool $premiacaoGeral): self {
        $this->premiacaoGeral = $premiacaoGeral;

        return $this;
    }

    public function getPremiacaoFaixa(): ?bool {
        return $this->premiacaoFaixa;
    }

    public function setPremiacaoFaixa(bool $premiacaoFaixa): self {
        $this->premiacaoFaixa = $premiacaoFaixa;

        return $this;
    }

    public function getNumero(): ?int {
        return $this->numero;
    }

    public function setNumero(int $numero): self {
        $this->numero = $numero;

        return $this;
    }

    public function getNome(): ?string {
        return $this->nome;
    }

    public function setNome(string $nome): self {
        $this->nome = $nome;

        return $this;
    }

    public function __toString() {
        return $this->nome;
    }

    public function getDistancia(): ?float
    {
        return $this->distancia;
    }

    public function setDistancia(?float $distancia): self
    {
        $this->distancia = $distancia;

        return $this;
    }
}
