<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CorridaRepository")
 */
class Corrida
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nome;

    /**
     * @ORM\Column(type="date")
     */
    private $data;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkRegulamento;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkOrganizador;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $imagem;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anexo",
     *     mappedBy="corrida",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     * )
     * @ORM\OrderBy({"categoria" = "ASC"})
     */
    private $anexos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Categoria", mappedBy="corrida" , cascade={"all"})
     * @ORM\OrderBy({"nome" = "ASC"})
     */
    private $categorias;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist"}, orphanRemoval=true)
     */
    private $imagemCertificado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tituloAnexos;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mostrarAnexosHome;

    public function __construct() {
        $this->categorias = new ArrayCollection();
        $this->anexos = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getNome(): ?string {
        return $this->nome;
    }

    public function setNome(string $nome): self {
        $this->nome = $nome;

        return $this;
    }

    public function getData(): ?\DateTimeInterface {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self {
        $this->data = $data;

        return $this;
    }

    public function getLinkRegulamento(): ?string {
        return $this->linkRegulamento;
    }

    public function setLinkRegulamento(?string $linkRegulamento): self {
        $this->linkRegulamento = $linkRegulamento;

        return $this;
    }

    public function getLinkOrganizador(): ?string {
        return $this->linkOrganizador;
    }

    public function setLinkOrganizador(?string $linkOrganizador): self {
        $this->linkOrganizador = $linkOrganizador;

        return $this;
    }

    public function getCidade(): ?string {
        return $this->cidade;
    }

    public function setCidade(?string $cidade): self {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagem() {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     * @return Corrida
     */
    public function setImagem($imagem) {
        $this->imagem = $imagem;
        return $this;
    }


    public function __toString() {
        return $this->nome ?? '';
    }

    /**
     * @return Collection|Categoria[]
     */
    public function getCategorias(): Collection {
        return $this->categorias;
    }

    public function addCategoria(Categoria $categoria): self {
        if (!$this->categorias->contains($categoria)) {
            $this->categorias[] = $categoria;
            $categoria->setCorrida($this);
        }

        return $this;
    }

    public function removeCategoria(Categoria $categoria): self {
        if ($this->categorias->contains($categoria)) {
            $this->categorias->removeElement($categoria);
            // set the owning side to null (unless already changed)
            if ($categoria->getCorrida() === $this) {
                $categoria->setCorrida(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Anexo[]
     */
    public function getAnexos(): Collection {
        return $this->anexos;
    }

    public function addAnexo(Anexo $anexo): self {
        if (!$this->anexos->contains($anexo)) {
            $this->anexos[] = $anexo;
            $anexo->setCorrida($this);
        }

        return $this;
    }

    public function removeAnexo(Anexo $anexo): self {
        if ($this->anexos->contains($anexo)) {
            $this->anexos->removeElement($anexo);
            // set the owning side to null (unless already changed)
            if ($anexo->getCorrida() === $this) {
                $anexo->setCorrida(null);
            }
        }

        return $this;
    }

    public function hasCategoria(): bool {
        return count($this->categorias) > 0;
    }

    public function hasAnexos(): bool {
        return count($this->anexos) > 0;
    }

    public function getImagemCertificado() {
        return $this->imagemCertificado;
    }

    public function setImagemCertificado($imagemCertificado): self {
        $this->imagemCertificado = $imagemCertificado;
        return $this;
    }

    public function getTituloAnexos(): ?string
    {
        return $this->tituloAnexos;
    }

    public function setTituloAnexos(?string $tituloAnexos): self
    {
        $this->tituloAnexos = $tituloAnexos;

        return $this;
    }

    public function getMostrarAnexosHome(): ?bool
    {
        return $this->mostrarAnexosHome;
    }

    public function setMostrarAnexosHome(?bool $mostrarAnexosHome): self
    {
        $this->mostrarAnexosHome = $mostrarAnexosHome;

        return $this;
    }
}
