<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181220190504 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anexo ADD corrida_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE anexo ADD CONSTRAINT FK_CD7EAF2C96D1CDC4 FOREIGN KEY (corrida_id) REFERENCES corrida (id)');
        $this->addSql('CREATE INDEX IDX_CD7EAF2C96D1CDC4 ON anexo (corrida_id)');
        $this->addSql('ALTER TABLE corrida DROP FOREIGN KEY FK_E096E0F3C9348664');
        $this->addSql('DROP INDEX UNIQ_E096E0F3C9348664 ON corrida');
        $this->addSql('ALTER TABLE corrida DROP anexo_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anexo DROP FOREIGN KEY FK_CD7EAF2C96D1CDC4');
        $this->addSql('DROP INDEX IDX_CD7EAF2C96D1CDC4 ON anexo');
        $this->addSql('ALTER TABLE anexo DROP corrida_id');
        $this->addSql('ALTER TABLE corrida ADD anexo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE corrida ADD CONSTRAINT FK_E096E0F3C9348664 FOREIGN KEY (anexo_id) REFERENCES anexo (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E096E0F3C9348664 ON corrida (anexo_id)');
    }
}
