<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218020431 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE anexo (id INT AUTO_INCREMENT NOT NULL, filename VARCHAR(255) NOT NULL, updated DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE corrida ADD anexo_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE corrida ADD CONSTRAINT FK_E096E0F3C9348664 FOREIGN KEY (anexo_id) REFERENCES anexo (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E096E0F3C9348664 ON corrida (anexo_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE corrida DROP FOREIGN KEY FK_E096E0F3C9348664');
        $this->addSql('DROP TABLE anexo');
        $this->addSql('DROP INDEX UNIQ_E096E0F3C9348664 ON corrida');
        $this->addSql('ALTER TABLE corrida DROP anexo_id');
    }
}
