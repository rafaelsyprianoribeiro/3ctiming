<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190915173608 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categoria_anexo (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE anexo ADD categoria_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE anexo ADD CONSTRAINT FK_CD7EAF2C3397707A FOREIGN KEY (categoria_id) REFERENCES categoria_anexo (id)');
        $this->addSql('CREATE INDEX IDX_CD7EAF2C3397707A ON anexo (categoria_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anexo DROP FOREIGN KEY FK_CD7EAF2C3397707A');
        $this->addSql('DROP TABLE categoria_anexo');
        $this->addSql('DROP INDEX IDX_CD7EAF2C3397707A ON anexo');
        $this->addSql('ALTER TABLE anexo DROP categoria_id');
    }
}
