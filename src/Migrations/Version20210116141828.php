<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116141828 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE corrida ADD imagem_certificado_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE corrida ADD CONSTRAINT FK_E096E0F3914E6B85 FOREIGN KEY (imagem_certificado_id) REFERENCES image (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E096E0F3914E6B85 ON corrida (imagem_certificado_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE corrida DROP FOREIGN KEY FK_E096E0F3914E6B85');
        $this->addSql('DROP INDEX UNIQ_E096E0F3914E6B85 ON corrida');
        $this->addSql('ALTER TABLE corrida DROP imagem_certificado_id');
    }
}
