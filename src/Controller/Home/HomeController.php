<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 15/08/2018
 * Time: 20:19
 */

namespace App\Controller\Home;

use App\Entity\Categoria;
use App\Entity\Corrida;
use App\Entity\Filter\CorridaFilter;
use App\Form\BuscaCorridaType;
use App\Manager\XlsManager;
use App\Repository\CorridaRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home")
     */
    public function index(
        Request $request,
        CorridaRepository $corridaRepository
    ): Response {
        /** @var Corrida $corridas */
        $corridas = $corridaRepository
            ->findComCategoriaOrderByDataDescQueryBuilder()
            ->setMaxResults(12)
            ->getQuery()
            ->getResult();

        $sliderCorridasQueryBuilder = $corridaRepository
            ->findSemCategoriaOrderByDataDescQueryBuilder()
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();

        if (count($sliderCorridasQueryBuilder) < 1) {
            $sliderCorridasQueryBuilder = [];
        }

        $corridasWithAnexosHome = $corridaRepository->findWithAnexosHomeOrderByDataDesc();
        $adapter = new DoctrineORMAdapter($corridasWithAnexosHome);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(3);
        try {
            $pagerfanta->setCurrentPage($request->query->get('page') ?? 1);
        } catch (\Exception $exception) {
            $pagerfanta->setCurrentPage(1);
        }


        return $this->render('home/index.html.twig', [
            'corridas' => $corridas,
            'sliderCorridas' => $sliderCorridasQueryBuilder,
            'corridasWithAnexosHome' => $pagerfanta
        ]);
    }

    /**
     * @Route("/lista-resultados", name="resultados")
     */
    public function resultados(
        Request $request,
        CorridaRepository $corridaRepository
    ) {

        $corridaFilter = new CorridaFilter();
        $formFilter = $this->createForm(BuscaCorridaType::class, $corridaFilter , [
            'method' => 'GET',
            'attr' => array(
                'class' => 'form-inline right-side'
            )
        ]);

        $formFilter->handleRequest($request);


        $corridasQueryBuilder = $corridaRepository
            ->findComCategoriaOrderByDataDescQueryBuilder($corridaFilter);
        $adapter = new DoctrineORMAdapter($corridasQueryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(12);
        try {
            $pagerfanta->setCurrentPage($request->query->get('page') ?? 1);
        } catch (\Exception $exception) {
            $pagerfanta->setCurrentPage(1);
        }
        return $this->render(
            'home/resultados.html.twig',
            [
                'form' => $formFilter->createView(),
                'corridas' => $pagerfanta
            ]
        );
    }

    /**
     * @Route("/{id}/resultado", name="resultado_corrida")
     */
    public function resultadoCorrida(Request $request, $id) {
        $corrida = $this->getDoctrine()->getManager()->getRepository("App:Corrida")->find($id);
        return $this->render('home/resultado_corrida.html.twig', ['corrida' => $corrida]);
    }

    /**
     * @Route("/categoria/{id}/download", name="categoria_download")
     */
    public function downloadCategoria(Categoria $categoria, XlsManager $xlsManager) {
        $file = $xlsManager->resutadosToXls($categoria);

        return new BinaryFileResponse($file);
    }

    /**
     * @Route("/proximascorridas", name="proximas")
     */
    public function proximas(Request $request) {


        $corridaFilter = new CorridaFilter();
        $formFilter = $this->createForm(BuscaCorridaType::class, $corridaFilter , [
            'method' => 'GET',
            'attr' => array(
                'class' => 'form-inline right-side'
            )
        ]);

        $formFilter->handleRequest($request);

        $corridasQueryBuilder = $this->getDoctrine()->getManager()->getRepository("App:Corrida")->findSemCategoriaOrderByDataDescQueryBuilder($corridaFilter);
        $adapter = new DoctrineORMAdapter($corridasQueryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(12);
        try {
            $pagerfanta->setCurrentPage($request->query->get('page') ?? 1);
        } catch (\Exception $exception) {
            $pagerfanta->setCurrentPage(1);
        }
        return $this->render(
            'home/proximas.html.twig',
            [
                'form' => $formFilter->createView(),
                'corridas' => $pagerfanta
            ]
        );
    }


}