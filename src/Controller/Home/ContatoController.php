<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 14/11/2018
 * Time: 23:17
 */

namespace App\Controller\Home;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContatoController extends AbstractController
{
    /**
     * @Route("/contato", name="contato", methods={"GET","POST"})
     */
    public function contato(Request $request, \Swift_Mailer $mailer)
    {
        $dados_contato = $this->getDoctrine()->getManager()->getRepository("App:Contato")
            ->findFirst();

        $email = $request->request->get('email');
        $nome = $request->request->get('nome');
        $celular = $request->request->get('celular');
        $mensagem = nl2br($request->request->get('mensagem'));

        $data['status'] = '';
        $data['dados_contato'] = $dados_contato;

        if($email && $mensagem) {
            $message = (new \Swift_Message('Hello Email'))
                ->setSubject('Contato 3cTiming')
                ->setFrom($dados_contato->getEmail())
                ->setTo($dados_contato->getEmail())
                ->setBody(
                    $this->renderView(
                        'home/corpo_email.html.twig',
                        array('nome' => $nome, 'email' => $email, 'celular' => $celular, 'mensagem' => $mensagem)
                    )
                    , 'text/html'
                );
            if($mailer->send($message)>0){
                $data['status'] = 'sucesso';
            }else{
                $data['status'] = 'erro';
            }
        }

        return $this->render('home/contato.html.twig', $data);
    }
}