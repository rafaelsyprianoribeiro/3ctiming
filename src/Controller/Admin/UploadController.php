<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 29/10/2018
 * Time: 18:55
 */

namespace App\Controller\Admin;


use App\Manager\UploadManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class UploadController extends Controller
{
    /**
     * @Route("/fileuploadhandler", name="fileuploadhandler")
     * @Template("/Admin/Categoria/table_list.html.twig")
     */
    public function fileUploadHandler(Request $request, UploadManager $uploadManager) {
        try {
            /** @var UploadedFile $file */
            $file = $request->files->get('file');
            $corridaId = $request->request->get('corridaId');
            $uploadManager->uploadResultado($file, $corridaId);
            $corrida = $this->getDoctrine()->getRepository('App:Corrida')
                ->find($corridaId);
            $categorias = $corrida->getCategorias();
            return [
                'categorias' => $categorias
            ];

        } catch (\Exception $exception) {
            return new JsonResponse("Arquivo incompatível.", 500);
        }
    }

    /**
     * @Route("/deletefileresource", name="deleteFileResource")
     */
    public function deleteResource(Request $request, UploadManager $uploadManager) {
        $fileName = $request->get('fileName');
        $corridaId = $request->get('corridaId');
        $output = $uploadManager->cancelUploadResultado($fileName, $corridaId);
        return new JsonResponse($output);
    }
}