<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 15/11/2018
 * Time: 18:02
 */

namespace App\Manager;


use App\DataTransformer\ArrayToResultadoDataTransformer;
use App\Entity\Categoria;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class XlsManager
{
    const DOWNLOAD_RESULTADO_DIR = '/../resultados/';

    private $arrayToResultadoDataTransform;

    private $rootDir;

    private $em;

    public function __construct($rootDir, ArrayToResultadoDataTransformer $arrayToResultadoDataTransform, EntityManagerInterface $em) {
        $this->rootDir = $rootDir;
        $this->arrayToResultadoDataTransform = $arrayToResultadoDataTransform;
        $this->em = $em;
    }

    public function resutadosToXls(Categoria $categoria): string {
        $spreadsheet = new Spreadsheet();

        $sheetGeral = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Geral');
        $spreadsheet->addSheet($sheetGeral, 0);
        $linhas = $this->resultadosGeral($categoria);
        $sheetGeral->fromArray($linhas, NULL, 'A1');

        $faixas = $this->em->getRepository('App:Resultado')
            ->findByCategoriaGroupByFaixa($categoria);
        foreach ($faixas as $faixa) {
            $sheetFaixa = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $faixa['faixa']);
            $spreadsheet->addSheet($sheetFaixa, 0);
            $linhas = $this->resultadosFaixa($categoria, $faixa['faixa']);
            $sheetFaixa->fromArray($linhas, NULL, 'A1');
        }

        $writer = new Xlsx($spreadsheet);
        $fileName = $this->rootDir . self::DOWNLOAD_RESULTADO_DIR . uniqid($categoria->getId()) . '.xlsx';
        $writer->save($fileName);
        return $fileName;
    }

    private function resultadosGeral(Categoria $categoria) {
        $linhas = [];
        $resultadosGeral = $this->em->getRepository('App:Resultado')
            ->findByCategoriaOrderByPosicaoGeral($categoria);
        foreach ($resultadosGeral as $resultado) {
            $arrayResultado = $this->arrayToResultadoDataTransform->reverseTransform($resultado);
            foreach ($arrayResultado as $key => $linhaResultado) {
                $linha[] = $key;
            }
            $linhas[] = $linha;
            break;
        }
        foreach ($resultadosGeral as $resultado) {
            $arrayResultado = $this->arrayToResultadoDataTransform->reverseTransform($resultado);
            $linha = [];
            foreach ($arrayResultado as $key => $linhaResultado) {
                $linha[] = $linhaResultado;
            }
            $linhas[] = $linha;
        }
        return $linhas;
    }

    private function resultadosFaixa(Categoria $categoria, string $faixa) {
        $linhas = [];
        $resultadosGeral = $this->em->getRepository('App:Resultado')
            ->findByCategoriaAndFaixaOrderByPosicaoFaixa($categoria, $faixa);
        foreach ($resultadosGeral as $resultado) {
            $arrayResultado = $this->arrayToResultadoDataTransform->reverseTransform($resultado);
            foreach ($arrayResultado as $key => $linhaResultado) {
                $linha[] = $key;
            }
            $linhas[] = $linha;
            break;
        }
        foreach ($resultadosGeral as $resultado) {
            $arrayResultado = $this->arrayToResultadoDataTransform->reverseTransform($resultado);
            $linha = [];
            foreach ($arrayResultado as $key => $linhaResultado) {
                $linha[] = $linhaResultado;
            }
            $linhas[] = $linha;
        }
        return $linhas;
    }
}