<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 29/10/2018
 * Time: 19:54
 */

namespace App\Manager;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadManager
{

    const UPLOAD_RESULTADO_DIR = '/../resultados/';

    private $rootDir;

    /** @var ResultadoManager  */
    private $resultadoManager;

    public function __construct($rootDir, ResultadoManager $resultadoManager) {
        $this->rootDir = $rootDir;
        $this->resultadoManager = $resultadoManager;
    }

    private function getUploadResultadoDir() {
        return $this->rootDir . $this::UPLOAD_RESULTADO_DIR;
    }

    public function uploadResultado(UploadedFile $file, $corridaId) {
        $output = ['uploaded' => false];
        $uploadDir = $this->getUploadResultadoDir();
        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }
        $newFile = $file->move($uploadDir, $file->getClientOriginalName());
        $this->resultadoManager->importCsvFile($newFile, $corridaId);
        $output['uploaded'] = true;
    }

    public function cancelUploadResultado($fileName, $corridaId): array {
        $output = ['deleted' => false, 'error' => false];
        if ($fileName) {
            $output['categoriaId'] = $this->resultadoManager->removeResultados($fileName, $corridaId);
            $output['deleted'] = true;
            if ($output['deleted']) {
            }
        } else {
            $output['error'] = 'Arquivo incorreto ou não encontrado.';
        }

        return $output;
    }
}