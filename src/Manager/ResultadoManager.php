<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 05/11/2018
 * Time: 15:05
 */

namespace App\Manager;


use App\DataTransformer\ArrayToResultadoDataTransformer;
use App\Entity\Categoria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\File;

class ResultadoManager
{

    /** @var EntityManagerInterface */
    private $em;

    private $arrayToResultadoDataTransform;

    public function __construct(EntityManagerInterface $em, ArrayToResultadoDataTransformer $arrayToResultadoDataTransform) {
        $this->em = $em;
        $this->arrayToResultadoDataTransform = $arrayToResultadoDataTransform;
    }

    private function valorToArray(array $campos, array $linha) {
        $valores = [];
        for ($i = 0; $i < count($campos); $i++) {
            $valores[$campos[$i]] = $linha[$i];
        }
        return $valores;
    }

    private function getNomeFormatado($fileName) {
        $nome = explode('.', $fileName);
        return str_replace('_', ' ', $nome[0]);
    }

    private function getKmFromNome($nome) {
        preg_match( '/([0-9]+)/', $nome, $matches);
        return $matches[0];
    }

    private function criaCategoria($nomeCategoria): Categoria {
        $categoria = new Categoria();
        $categoria->setNome($nomeCategoria);
//        $categoria->setDistancia($this->getKmFromNome($nomeCategoria));
        return $categoria;
    }

    private function getFileNameFromFile(File $file) {
        $pathParts = pathinfo($file->getRealPath());
        return $this->getNomeFormatado($pathParts['filename']);
    }

    public function importCsvFile(File $file, $corridaId): void {
        $corrida = $this->em->getRepository('App:Corrida')
            ->find($corridaId);

        $nomeCategoria = $this->getFileNameFromFile($file);

        $categoria = $this->em->getRepository('App:Categoria')->findOneByCorridaAndNomeCategoria($corrida, $nomeCategoria);
        if ($categoria) {
            $this->em->remove($categoria);
            $this->em->flush();
        }

        $categoria = $this->criaCategoria($nomeCategoria);

        ini_set('auto_detect_line_endings', TRUE);
        $handle = fopen($file->getRealPath(), "r");
        $campos = fgetcsv($handle, null, ';');
        while (($data = fgetcsv($handle, null, ';')) !== FALSE) {
            $valores = $this->valorToArray($campos, $data);
            $resultado = $this->arrayToResultadoDataTransform->transform($valores);
            $categoria->addResultado($resultado);
            $distancia = $resultado->getDistancia();
            $this->em->persist($resultado);
        }
        $categoria->setDistancia($distancia);
        $categoria->setCorrida($corrida);
        $this->em->persist($categoria);
        $this->em->flush();
        ini_set('auto_detect_line_endings', FALSE);
    }

    public function removeResultados($fileName, $corridaId): int {
        $corrida = $this->em->getRepository('App:Corrida')
            ->find($corridaId);

        $nomeCategoria = $this->getNomeFormatado($fileName);
        /** @var Categoria $categoria */
        $categoria = $this->em->getRepository('App:Categoria')
            ->findOneByCorridaAndNomeCategoria($corrida, $nomeCategoria);

        if (!$categoria)
            throw new \Exception('Categoria não encontrada');

        $categoriaId = $categoria->getId();
        $this->em->remove($categoria);
        $this->em->flush();

        return $categoriaId;
    }

    public function exportCsvFile() {
        
    }
}