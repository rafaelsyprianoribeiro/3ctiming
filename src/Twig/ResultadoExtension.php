<?php
/**
 * Created by PhpStorm.
 * User: rafael.s.ribeiro
 * Date: 18/11/2018
 * Time: 13:26
 */

namespace App\Twig;


use App\Entity\Categoria;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ResultadoExtension extends AbstractExtension
{

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function getFunctions() {
        return array(
            new TwigFunction('resultado_geral', array($this, 'resultadoGeral')),
            new TwigFunction('resultado_faixa', array($this, 'resultadoFaixa')),
            new TwigFunction('faixas_resultado', array($this, 'faixasResultado')),
            new TwigFunction('decimal_to_time', array($this, 'decimalToTime')),
            new TwigFunction('time_to_decimal', array($this, 'timeToDecimal')),
        );
    }

    public function resultadoGeral(Categoria $categoria) {
        return $this->em->getRepository('App:Resultado')
            ->findByCategoriaOrderByPosicaoGeral($categoria);
    }

    public function resultadoFaixa(Categoria $categoria, string $faixa) {
        return $this->em->getRepository('App:Resultado')
            ->findByCategoriaAndFaixaOrderByPosicaoFaixa($categoria, $faixa);
    }

    public function faixasResultado(Categoria $categoria) {
        return $this->em->getRepository('App:Resultado')
            ->findByCategoriaGroupByFaixa($categoria);
    }

    public function decimalToTime($decimal) {
        $hours = floor($decimal / 60);
        $minutes = floor($decimal % 60) + ($hours * 60);
        $seconds = $decimal - (int)$decimal;
        $seconds = round($seconds * 60);

        return str_pad($minutes, 2, "0", STR_PAD_LEFT) . ":" . str_pad($seconds, 2, "0", STR_PAD_LEFT);
    }

    public function timeToDecimal($time) {
        $hms = explode(":", $time);
        $hora = $hms[0] > 0 ? ($hms[0]*60) : 0;
        $minutos = $hms[1];
        $segundos = ($hms[2]/60);
        return ($hora + $minutos + $segundos);
    }

}