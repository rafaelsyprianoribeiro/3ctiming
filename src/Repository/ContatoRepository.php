<?php

namespace App\Repository;

use App\Entity\Contato;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Contato|null find($id, $lockMode = null, $lockVersion = null)
 */
class ContatoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contato::class);
    }

    public function findFirst() {
        return $this->createQueryBuilder('contato')
            ->setMaxResults(1)
            ->orderBy('contato.id')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
