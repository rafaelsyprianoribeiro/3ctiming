<?php

namespace App\Repository;

use App\Entity\Categoria;
use App\Entity\Corrida;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Categoria|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categoria|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categoria[]    findAll()
 * @method Categoria[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categoria::class);
    }

    public function findOneByCorridaAndNomeCategoria(Corrida $corrida, string $nomeCategoria) {
        return $this->createQueryBuilder('categoria')
            ->join('categoria.corrida', 'corrida')
            ->andWhere('corrida = :corrida')
            ->andWhere('categoria.nome = :nomeCategoria')
            ->setParameter('corrida', $corrida)
            ->setParameter('nomeCategoria', $nomeCategoria)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
