<?php

namespace App\Repository;

use App\Entity\Anexo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Anexo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Anexo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Anexo[]    findAll()
 * @method Anexo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnexoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Anexo::class);
    }

}
