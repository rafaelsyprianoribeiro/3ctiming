<?php

namespace App\Repository;

use App\Entity\Corrida;
use App\Entity\Filter\CorridaFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Corrida|null find($id, $lockMode = null, $lockVersion = null)
 * @method Corrida|null findOneBy(array $criteria, array $orderBy = null)
 * @method Corrida[]    findAll()
 * @method Corrida[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorridaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Corrida::class);
    }


    public function findComCategoriaOrderByDataDescQueryBuilder(CorridaFilter $corridaFilter = null) {

        $qb = $this->createQueryBuilder('corrida')
            ->join('corrida.categorias', 'categorias');
        if($corridaFilter != null && $corridaFilter->getNome()){
            $qb->andWhere('LOWER(corrida.nome) LIKE LOWER(:nome)')
                ->orWhere('LOWER(corrida.cidade) LIKE LOWER(:cidade)')
                ->setParameter('nome', '%' . $corridaFilter->getNome() . '%')
                ->setParameter('cidade', '%' . $corridaFilter->getNome() . '%');
        }
        if($corridaFilter != null && $corridaFilter->getData()){
            $qb->andWhere('corrida.data = :data')
            ->setParameter('data',$corridaFilter->getData());
        }


        return
            $qb->orderBy('corrida.data', 'DESC');
    }

    public function findSemCategoriaOrderByDataDescQueryBuilder(CorridaFilter $corridaFilter = null) {

        $qb = $this->createQueryBuilder('corrida')
            ->leftJoin('corrida.categorias', 'categorias');
        if($corridaFilter != null && $corridaFilter->getNome()){
            $qb->andWhere('LOWER(corrida.nome) LIKE LOWER(:nome)')
                ->orWhere('LOWER(corrida.cidade) LIKE LOWER(:cidade)')
                ->setParameter('nome', '%' . $corridaFilter->getNome() . '%')
                ->setParameter('cidade', '%' . $corridaFilter->getNome() . '%');
        }

        if($corridaFilter != null && $corridaFilter->getData()){
            $qb->andWhere('corrida.data = :data')
                ->setParameter('data',$corridaFilter->getData());
        }

        return $qb->andWhere('categorias is null')
            ->orderBy('corrida.data', 'DESC');
    }

    public function findWithAnexosHomeOrderByDataDesc(int $maxResult = 10) {
        return $this->createQueryBuilder('corrida')
            ->where('corrida.mostrarAnexosHome = true')
            ->orderBy('corrida.data', 'DESC')
        ;
    }

}
