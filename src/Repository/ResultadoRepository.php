<?php

namespace App\Repository;

use App\Entity\Categoria;
use App\Entity\Resultado;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Resultado|null find($id, $lockMode = null, $lockVersion = null)
 * @method Resultado|null findOneBy(array $criteria, array $orderBy = null)
 * @method Resultado[]    findAll()
 * @method Resultado[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultadoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Resultado::class);
    }

    public function findByCategoriaOrderByPosicaoGeral(Categoria $categoria) {
        return $this->createQueryBuilder('resultado')
            ->where('resultado.categoria = :categoria')
            ->orderBy('resultado.posicaoGeral', 'ASC')
            ->setParameter('categoria', $categoria)
            ->getQuery()
            ->getResult();
    }

    public function findByCategoriaAndFaixaOrderByPosicaoFaixa(Categoria $categoria, string $faixa) {
        return $this->createQueryBuilder('resultado')
            ->andWhere('resultado.categoria = :categoria')
            ->andWhere('resultado.faixa = :faixa')
            ->orderBy('resultado.posicaoFaixa', 'ASC')
            ->setParameter('categoria', $categoria)
            ->setParameter('faixa', $faixa)
            ->getQuery()
            ->getResult();
    }

    public function findByCategoriaGroupByFaixa(Categoria $categoria) {
        return $this->createQueryBuilder('resultado')
            ->select('resultado.faixa')
            ->andWhere('resultado.categoria = :categoria')
            ->setParameter('categoria', $categoria)
            ->groupBy('resultado.faixa')
            ->getQuery()
            ->getArrayResult();
    }
}
