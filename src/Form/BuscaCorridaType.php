<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class BuscaCorridaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->addFields($builder, $options);
    }

    private function addFields(FormBuilderInterface $builder, array $options) {
        $this->addNome($builder);
        $this->addData($builder);
        $this->addButton($builder);
    }


    private function addNome(FormBuilderInterface $builder) {
        $builder->add('nome', TextType::class, array(
                'label' => 'Corrida',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Corrida, Cidade',
                )
            )
        );
    }

    private function addData(FormBuilderInterface $builder) {
        $builder->add('data', DateType::class, array(
                'label' => 'Data',
                'widget' => 'single_text',
                'required' => false,
                'html5' => true,
                'attr' => array(
                    'class' => 'form-control js-datepicker'
                )
            )
        );
    }

    private function addButton(FormBuilderInterface $builder) {
        $builder->add('submit', SubmitType::class, array(
            'attr' => array('class' => 'btn btn-dark mb-2'),
            'label' => 'Buscar'
        ));

    }


    public function getName() {
        return 'buscacorrida';
    }

    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Filter\CorridaFilter',
        ));
    }
}
