$(document).ready(function () {
    // init,configure dropzone
    Dropzone.autoDiscover = false;
    var dropzone_default = new Dropzone(".dropzone", {
        url: '/fileuploadhandler',
        maxFilesize: 5,
        addRemoveLinks: true,
        dictDefaultMessage: "Arraste o arquivo ou clique para adicionar...",
        init: function () {
            this.on("maxfilesexceeded", function (file) {
                this.removeFile(file);
            });
            this.on("sending", function (file, xhr, formData) {
                // send additional data with the file as POST data.
                var corridaId = $('#corridaId').val();
                formData.append("corridaId", corridaId);
            });
            this.on("success", function (file, response) {
                var table_categoria = $('#table-categorias')
                table_categoria.html(response)
                $('#fileName').val(response.fileName);
            });
            this.on("removedfile", function (file) {
                // Delete file from server
                $.ajax({
                    type: 'POST',
                    url: '/deletefileresource',
                    data: {
                        fileName: file.name,
                        corridaId: $('#corridaId').val()
                    },
                    dataType: 'json'
                }).done(function (resp) {
                    if (resp.deleted) {
                        $.notify({
                            message: 'Arquivo removido: <b>' + file.name + '</b>'
                        },{
                            type: 'success'
                        });
                        var acord_categoria = $('.categoria'+resp.categoriaId)
                        acord_categoria.html("")
                    } else if (resp.error) {
                        $.notify({
                            message: resp.error
                        },{
                            type: 'error'
                        });
                    }
                }).fail(function (resp) {
                    return false;
                });
            });
            // this event is required only to debug errors
            this.on("error", function (file, errorMessage) {
                $.notify({
                    message: 'Erro ao fazer o upload do arquivo'
                },{
                    type: 'error'
                });
            });
        }
    });
});