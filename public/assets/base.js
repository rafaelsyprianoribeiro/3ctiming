$(function () {
    $(".btn-certificado").on('click', function () {

        var tempoBruto = $(this).data('tempo-bruto').split(":");
        $("#tempo-bruto-certificado span").each(function (i, elem) {
            if (parseInt(tempoBruto[i].trim()) > 0) {
                $(elem).html(tempoBruto[i].trim());
            } else {
                $(elem).html("00");
            }
        });
        var tempoLiquido = $(this).data('tempo-liquido').split(":");
        $("#tempo-liquido-certificado span").each(function (i, elem) {
            if (parseInt(tempoLiquido[i].trim()) > 0) {
                $(elem).html(tempoLiquido[i].trim());
            } else {
                $(elem).html("00");
            }
        });
        $("#nome-certificado").html($(this).data('nome'));
        $("#evento-certificado").html($(this).data('corrida-nome'));
        $("#ritmo-certificado").html($(this).data('ritmo'));
        $("#velocidade-certificado").html($(this).data('velocidade'));
        $("#geral-certificado").html($(this).data('geral'));
        $("#faixa-certificado").html($(this).data('faixa'));
        $("#modalidade-certificado").html($(this).data('modalidade'));
        $("#faixa-etaria-certificado").html($(this).data('faixa-etaria'));
        $("#equipe-certificado").html($(this).data('equipe'));
        $("#").html($(this).data('nome'));
        window.print();
    });
});